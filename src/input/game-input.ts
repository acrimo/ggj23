/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameScene, GameState } from "../scenes/game-scene"
import { posToI, posToJ } from "../map/coordinates";

import {
    SQR_TRESHOLD_DRAG
} from "../config/const";

export class GameInput {
    private scene: GameScene;

    private xClick: number | null = null;
    private yClick: number | null = null;

    constructor(scene: GameScene) {
        // markers
        scene.input.addListener("pointerup", this.pointerUp, this);

        scene.input.addListener("pointermove", this.pointerMove, this);
        scene.input.addListener("pointerdown", this.pointerDown, this);

        scene.input.addListener("gameout", this.pointerOut, this);

        this.scene = scene;
    }

    // #region PointerCallbacks

    private pointerMove(): void {
        if (this.scene.getState() == GameState.BUILD) {
            // if you have click down (xclick != null) and move a significant amout start draggin map
            if (this.xClick !== null && this.yClick !== null &&
                Phaser.Math.Distance.Squared(
                    this.scene.input.x, this.scene.input.y,
                    this.xClick, this.yClick
                ) > SQR_TRESHOLD_DRAG) {
                this.scene.startDrag();
                this.drag();
            } else {
                // otherwise move the marker to closest node available
                let x = this.scene.input.x + this.scene.cameras.main.scrollX;
                let y = this.scene.input.y + this.scene.cameras.main.scrollY;

                let j = posToJ(y);
                if (j >= 0) // underground check
                    this.scene.moveMarker(posToI(x), j);
            }
        } else if (this.scene.getState() == GameState.DRAG) {
            this.drag();
        }
    }

    private pointerDown(): void {
        if (this.scene.getState() == GameState.BUILD) {
            // save the position to calculate drag
            this.xClick = this.scene.input.x;
            this.yClick = this.scene.input.y;
        }
    }

    private pointerUp(): void {
        if (this.scene.getState() == GameState.BUILD) {
            this.scene.placeMarker();
        } else if (this.scene.getState() == GameState.DRAG) { // end drag
            this.drag();
            this.scene.startBuild();
        }

        // reset click down position in both cases
        this.xClick = null;
        this.yClick = null;
    }

    private pointerOut(): void {
        // if we move pointer out of the game frame
        if (this.scene.getState() == GameState.DRAG) {
            this.drag();
            this.scene.startBuild();

            this.xClick = null;
            this.yClick = null;
        }
    }

    //#endregion

    private drag(): void {
        if (this.xClick !== null && this.yClick !== null) {
            let x = this.scene.input.x - this.xClick;
            let y = this.scene.input.y - this.yClick;

            this.scene.moveCamera(x, y);

            this.xClick = this.scene.input.x;
            this.yClick = this.scene.input.y;
        }
    }

    // private createArrows(): void {
    //     this.input.keyboard.on("keydown-U", () => {
    //         if (this.state == GameState.BUILD) {
    //             // this.tree.goUp();
    //             this.state = GameState.UPGRADE;

    //             this.grid.pointerClear();
    //         }
    //         else if (this.state == GameState.UPGRADE)
    //             // this.tree.goDown();
    //     });
    // }
}