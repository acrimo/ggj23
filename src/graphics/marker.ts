
import { Point, posToI, posToJ } from "../map/coordinates";
import { MapGraphics } from "./map-graphics";

export class Marker {
    private images: Phaser.GameObjects.Image[];

    constructor(scene: Phaser.Scene, graphics: MapGraphics) {
        this.images = new Array<Phaser.GameObjects.Image>(2);

        this.images[0] = graphics.createMarker().setVisible(false);
        this.images[1] = graphics.createMarker().setVisible(false);
    }

    /**
     * Move first marker to the position (x, y)
     */
    public move(x: number, y: number): void {
        if (!this.images[0].visible) this.images[0].setVisible(true);
        this.images[0].setPosition(x, y);
    }

    /**
     * Locks the first marker into the position that already is in
     */
    public select(): void {
        if (this.images[1].visible == false) {
            this.images[1].setPosition(this.images[0].x, this.images[0].y);
            this.images[1].setVisible(true);

            this.hideFirst();
        }
    }

    // public distanceCheck(lenght: number): boolean {
    //     var dis = lenght * lenght;
    //     var sqr = Phaser.Math.Distance.Squared(this.images[0].x,
    //         this.images[0].y, this.images[1].x, this.images[1].y);

    //     return (sqr < dis);
    // }

    /**
     * Resets the first marker
     */
    public hideFirst(): void {
        this.images[0].visible = false;
    }

    /**
     * Reset both markers
     */
    public clear(): void {
        this.images[0].visible = false;
        this.images[1].visible = false;
    }

    /**
     * Checks if the marker has a viable root marked.
     * If image number 2 is visible a first root was already selected.
     * If image number 1 is visible the pointer is already on top of a node that
     * can be connected with the marked by image 2
     */
    public rootReady(): boolean {
        return this.images[0].visible && this.images[1].visible;
    }

    /**
     * Checks if a node is locked as the first one
     */
    public isFirstLocked() { return this.images[1].visible }

    /**
     * Checks if pointer is on top of a viable root start node
     */
    public isPointerOnFirst(): boolean {
        return this.images[0].visible // first image got active on the move check
            && !this.images[1].visible; // second image is not locked already
    }

    /**
     * Get the coordinates of the first node marked
     */
    public getLockedPosition(): Point {
        return {
            x: posToI(this.images[1].x),
            y: posToJ(this.images[1].y)
        }
    }

    /**
     * Get the coordinates of the first node marked
     */
    public getSecondPosition(): Point {
        return {
            x: posToI(this.images[0].x),
            y: posToJ(this.images[0].y)
        };
    }
}