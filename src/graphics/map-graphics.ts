/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameObjects } from "phaser";

import { GameScene } from "../scenes/game-scene"
import { Marker } from "./marker";

import {
    HEX_COLUMNS_COUNT,
    HEX_LINE,
    HEX_ROW,
    HEX_ROWS_COUNT,
    HEX_WIDTH
} from "../config/const";

import {
    BACKGROUND_LAYER,
    COLLECTABLE_LAYER,
    HEXAGONS_LAYER,
    MARKER_LAYER,
    ROOT_LAYER,
    STONE_LAYER,
    TREE_LAYER
} from "../config/layers"

export class MapGraphics {
    private scene: GameScene;
    private graphics: GameObjects.Graphics;

    constructor(scene: GameScene) {
        this.scene = scene;

        this.graphics = scene.add.graphics();
        this.graphics.lineStyle(8, 0xffff00);
        this.graphics.setDepth(ROOT_LAYER);

        this.loadBackground();
        this.loadHexagons();
    }

    private loadBackground(): void {
        this.scene.add.tileSprite(-200, -150, 4096, 4096, "dirt").setOrigin(0)
            .setDepth(BACKGROUND_LAYER);
        this.scene.add.image(
            (HEX_COLUMNS_COUNT * .5 + .5) * (HEX_WIDTH - HEX_LINE), -25, "tree")
            .setOrigin(0.5, 1).setDepth(TREE_LAYER);
    }

    /**
     * Loads the hexagons to form the background using the size given on constructor.
     * Starts placing on 0, 0(top left corner) to simplify calculations on clicks.
     */
    private loadHexagons(): void {
        // let x = 0;
        // let y = 0;

        // for (let i = 0; i < this.width; i++)
        //     for (let j = 0; j < this.height; j++) {
        //         this.scene.add.image(xPosHexa(i, j), yPosHexa(j), "hexagon").setOrigin(0, 0);
        //     }

        let tile = this.scene.add.tileSprite(-1, 0,
            (HEX_WIDTH - HEX_LINE) * HEX_COLUMNS_COUNT + 2,
            (66 + HEX_ROW) * HEX_ROWS_COUNT, "hex-tile")
            .setOrigin(0).setTilePosition(-1, 0)
            .setDepth(HEXAGONS_LAYER);
    }

    public addWater(x: number, y: number): Phaser.GameObjects.Image {
        let water = this.scene.add.image(x, y, "water")
            .setOrigin(0)
            .setDepth(COLLECTABLE_LAYER);
        return water;
    }

    public addNutrient(x: number, y: number): Phaser.GameObjects.Image {
        let nutrient = this.scene.add.image(x, y, "nutrient")
            .setOrigin(0)
            .setDepth(COLLECTABLE_LAYER);
        return nutrient;
    }

    public addStone(x: number, y: number): Phaser.GameObjects.Image {
        let stone = this.scene.add.image(x, y, "rock")
            .setScale(0.2)
            .setDepth(STONE_LAYER);
        return stone;
    }

    public createMarker(): Phaser.GameObjects.Image {
        let marker = this.scene.add.image(0, 0, "marker")
            .setDepth(MARKER_LAYER);

        return marker;
    }

    public addRoot(x0: number, y0: number, x1: number, y1: number): void {
        this.graphics.lineBetween(x0, y0, x1, y1);
    }
}