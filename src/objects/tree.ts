
import { HEX_WIDTH, HEX_LINE } from "../config/const";
import { GameScene } from "../scenes/game-scene"
import Phaser, { Tweens } from "phaser";
import { UI } from "./ui";
import { LevelController } from "../controllers/level";

const PRICE = [10, 10, 30, 20, 10];

export class Tree {
    private scene: GameScene;
    private background: Phaser.GameObjects.Image[];
    private upgradeTree: Phaser.GameObjects.Image;

    private upgrades: Phaser.GameObjects.Image[];
    private upgradeChart: Phaser.GameObjects.Image;
    private upgradeButton: Phaser.GameObjects.Image;
    private price: Phaser.GameObjects.BitmapText;

    private upgradesBought = [false, false, false, false, false];
    private selected = -1;

    private nextDayButton: Phaser.GameObjects.Image;

    private tween: Phaser.Tweens.Tween;
    private up: boolean;

    private level: LevelController;
    private ui: UI;

    constructor(scene: GameScene, level: LevelController, ui: UI) {
        this.scene = scene;
        this.level = level;
        this.ui = ui;

        this.background = new Array<Phaser.GameObjects.Image>(6);
        for (let i = 0; i < 6; i++)
            this.background[i] = scene.add.image(0, -960, "tree-back-" + i).setOrigin(0).setScrollFactor(0).setDepth(10 + i);

        this.upgradeTree = scene.add.image(0, -960, "tree-upgrade").setOrigin(0).setScrollFactor(0).setDepth(13);

        this.tween = scene.tweens.addCounter({
            from: 0,
            to: 1,
            ease: "Cubic.Out",
            duration: 2000,
            onUpdate: this.tweenUpdate,
            onComplete: this.tweenComplete,
            paused: true
        });

        this.upgrades = new Array<Phaser.GameObjects.Image>(5);

        for (let i = 0; i < 5; i++) {
            this.upgrades[i] = scene.add.image(200, 200, "upgrade").setScrollFactor(0).setDepth(14).setVisible(false);
            this.upgrades[i].on("pointerdown", () => { this.openUpgrade(i); }).setInteractive();
        }

        this.upgrades[0].setPosition(100, 525);
        this.upgrades[1].setPosition(155, 362);
        this.upgrades[2].setPosition(289, 310);
        this.upgrades[3].setPosition(413, 348);
        this.upgrades[4].setPosition(486, 455);

        this.nextDayButton = scene.add.image(540 * .5, 960 - 200, "next-day-button").setScrollFactor(0).setVisible(false).setDepth(30);
        this.nextDayButton.setInteractive().on("pointerdown", this.goDown, this);

        this.upgradeChart = scene.add.image(540 * .5, 550, "upgrade-0").setScrollFactor(0).setDepth(30).setScale(0.8).setVisible(false);
        this.upgradeButton = scene.add.image(540 * .5, 960 * .5 + 100 + 50, "buy-button")
            .setScrollFactor(0).setDepth(30).setVisible(false).setInteractive();
        this.price = scene.add.bitmapText(540 * .5 - 50, 960 * .5 + 85 + 50, "font", "00", 35)
            .setScrollFactor(0).setTint(0x4C4235).setDepth(30).setVisible(false);

        this.upgradeButton.on("pointerdown", this.buy, this);
    }

    public goUp(): void {
        this.up = true;
        this.tween.play();
    }

    public goDown(): void {
        for (let i = 0; i < 5; i++)
            this.upgrades[i].visible = false;

        this.nextDayButton.setVisible(false);

        this.upgradeChart.setVisible(false);
        this.upgradeButton.setVisible(false);
        this.price.setVisible(false);

        let moves = 15 + (this.upgradesBought[0] ? 10 : 0);
        this.ui.setMoveCount(moves);
        this.level.movementCount = moves;

        setTimeout(() => {
            this.up = false;
            this.tween.play();
        }, 500);
    }

    private tweenUpdate() {

        if (this.up) {
            for (let i = 1; i < 6; i++)
                this.background[i].y = (-960 - (6 - i) * 200) + (960 + (6 - i) * 200) * this.tween.getValue();

            this.background[0].y = this.background[5].y;
            this.upgradeTree.y = this.background[3].y;
        } else {
            for (let i = 1; i < 6; i++)
                this.background[i].y = (-960 - (6 - i) * 200) * this.tween.getValue();

            this.background[0].y = this.background[5].y;
            this.upgradeTree.y = this.background[3].y;
        }
    }

    private tweenComplete() {
        if (this.up) {
            for (let i = 0; i < 5; i++)
                this.upgrades[i].visible = true;

            this.nextDayButton.setVisible(true);

        } else {
            this.scene.endUpgrade();
        }
    }

    private openUpgrade(index: number): void {
        this.upgradeChart.setTexture("upgrade-" + index);
        this.upgradeChart.setVisible(true);

        this.upgradeButton.setVisible(true);

        this.price.setText(PRICE[index].toString());
        this.price.setVisible(true);

        this.selected = index;
    }

    private buy() {
        this.level.nutrientCount -= PRICE[this.selected];
        this.ui.setNutrientCount(this.level.nutrientCount);

        this.upgradesBought[this.selected] = true;
        this.upgrades[this.selected].setTint(0x00ff00);
        this.upgrades[this.selected].removeInteractive();

        this.upgradeChart.setVisible(false);
        this.upgradeButton.setVisible(false);
        this.price.setVisible(false);
    }
}