/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameScene } from "../scenes/game-scene"
import { xToPos, yToPos } from "../map/coordinates";
import { isEven } from "../utils/misc";

export class Stones {
    private scene: GameScene;
    private stones: Stone[];

    constructor(scene: GameScene) {
        this.scene = scene;
        this.stones = new Array<Stone>();
    }

    public add(x: number, y: number) {
        let stone = new Stone(x, y);
        this.stones.push(stone);
        let xPos = xToPos(stone.x);
        let yPos = yToPos(stone.y, isEven(stone.x));
        this.scene.add.image(xPos, yPos, Math.round(Math.random()) ? "rock" : "skeleton").setScale(0.30);
    }

    public hasStone(x: number, y: number) {
        return this.stones.some((val: Stone) => val.x === x && val.y === y);
    }
}

export class Stone {
    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}
