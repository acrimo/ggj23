/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

export class Menu extends Phaser.GameObjects.Container {
    private buttons: Array<Button>;
    private pressed: number = -1;

    constructor(scene: Phaser.Scene) {
        super(scene, 0, 0);
        this.buttons = new Array<Button>();
        scene.add.existing(this);
    }

    public addScaleButton(x: number, y: number, texture: string,
        scale: number, callback: () => void) {
        let image = this.scene.add.image(x, y, texture);
        this.add(image);

        let index = this.buttons.push(new ScaleImageButton(
            callback, image, scale)) - 1;

        image.setInteractive();
        image.on('pointerover', () => this.onPointerOver(index));
        image.on('pointerout', () => this.onPointerOut(index));
        image.on('pointerdown', () => this.onPointerDown(index));
        image.on('pointerup', () => this.onPointerUp(index));
    }

    private onPointerOver(index: number): void {
        if (this.pressed == -1) {
            this.buttons[index].hover();
        } else if (this.pressed == index) {
            this.buttons[index].down();
        }
    }

    private onPointerOut(index: number): void {
        this.buttons[index].out();
    }

    private onPointerDown(index: number): void {
        this.pressed = index;
        this.buttons[index].down();

    }

    private onPointerUp(index: number): void {
        if (this.pressed == index) {
            this.buttons[index].up();
            this.buttons[index].callback();
        }
    }
}

class Button {
    public callback;
    constructor(callback: () => void) {
        this.callback = callback;
    }

    // callbacks
    public hover(): void { }
    public out(): void { }
    public down(): void { }
    public up(): void { }
}

class ScaleImageButton extends Button {
    private image: Phaser.GameObjects.Image;
    private scale: number;

    constructor(callback: () => void,
        image: Phaser.GameObjects.Image, scale: number) {
        super(callback);
        this.image = image;
        this.scale = scale;
    }

    public hover() {
        this.image.setScale(1 + this.scale);
    }

    public out() {
        this.image.setScale(1);
    }

    public down() {
        this.image.setScale(1 - this.scale);
    }

    public up() {
        this.image.setScale(1);
    }
}

// class ImageButton extends Button {
//     private image: Phaser.GameObjects.Image;

//     constructor(image: Phaser.GameObjects.Image,
//         idle: string, hover: string, down: string, up: string) {
//         super();
//     }
// }