/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameScene } from "../scenes/game-scene";

import { UI_LAYER } from "../config/layers";

export class UI {
    private waterFill: Phaser.GameObjects.Image;
    private waterDead: Phaser.GameObjects.Image;

    private nutrientCount: Phaser.GameObjects.BitmapText;
    private moveCount: Phaser.GameObjects.BitmapText;

    public upgrade: boolean = false;

    constructor(scene: GameScene) {
        scene.add.image(320 + 8, 29 + 16 + 8, "ui-water-background")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        this.waterFill = scene.add.image(320 + 8, 29 + 16 + 8, "ui-water-fill")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        this.waterDead = scene.add.image(320 + 8, 29 + 16 + 8, "ui-water-dead")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);

        scene.add.image(28 + 8, 29 + 16, "ui-container-small")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        scene.add.image(104 + 10, 29 + 16, "ui-container-small")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        scene.add.image(320, 29 + 16, "ui-container-big")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);

        scene.add.image(480, 31, "ui-water")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        scene.add.image(160 + 10, 40, "ui-moves")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);
        scene.add.image(16, 37, "ui-nutrient")
            .setOrigin(0).setScrollFactor(0).setDepth(UI_LAYER);


        this.setFilledPerc(0);

        this.nutrientCount = scene.add.bitmapText(58, 58, "font", "0", 20)
            .setScrollFactor(0).setTint(0x4C4235).setDepth(UI_LAYER);
        this.moveCount = scene.add.bitmapText(132, 58, "font", "40", 20)
            .setScrollFactor(0).setTint(0x4C4235).setDepth(UI_LAYER);

    }

    public setMoveCount(value: number): void {
        this.moveCount.setText((value < 10 ? "0" : "") + value);
    }

    public setNutrientCount(value: number): void {
        this.nutrientCount.setText((value < 10 ? "0" : "") + value);
    }

    public setFilledPerc(value: number): void {
        this.waterFill.scaleX = value * 0.05 * 170;
    }

    public setDeadZone(value: number) {
        this.waterDead.x = 320 + 8 + value * 0.05 * 170;
    }
}