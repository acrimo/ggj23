// this should have another name

/**
 * Handles the information on the points where nutrients and water 
 * can be placed (the hexagons centers)
 */
export class Hexagons {
    private hexagons: Hexagon[][];

    /**
     * Width and height provided referes to the amount of hexagons
     */
    constructor(width: number, height: number) {
        this.hexagons = new Array<Array<Hexagon>>(width)
        for (let i = 0; i < width; i++) {
            this.hexagons[i] = new Array<Hexagon>(height);
            for (let j = 0; j < height; j++) {
                this.hexagons[i][j] = new Hexagon();
            }
        }
    }

    public addWater(i: number, j: number, image: Phaser.GameObjects.Image) {
        this.hexagons[i][j].content = 1;
        this.hexagons[i][j].image = image;
    }

    public addNutrient(i: number, j: number, image: Phaser.GameObjects.Image) {
        this.hexagons[i][j].content = 2;
        this.hexagons[i][j].image = image;

    }

    public consume(i: number, j: number): number {
        if (i < 0 || j < 0) return -1;
        if (this.hexagons[i][j].image !== null) {
            this.hexagons[i][j].image.destroy();
            this.hexagons[i][j].image = null;

            return this.hexagons[i][j].content;
        }

        return -1;
    }

}

export class Hexagon {
    public soil: number = 0;
    public content: number = 0;

    public image: Phaser.GameObjects.Image = null;
}
