import { HEX_WIDTH, HEX_HEIGHT, HEX_LINE, HEX_ROW } from "../config/const";
import { isEven } from "../utils/misc";

/**
 * Get the xPos (physic) of the i-th j-th Hexa
 */
export const xPosHexa = (i: number, j: number): number => {
    return isEven(j) ? (HEX_WIDTH - HEX_LINE) * i : (HEX_WIDTH - HEX_LINE) * (i - .5);
}

/**
 * Get the yPos (physic) of the j-th Hexa
 */
export const yPosHexa = (j: number): number => {
    return (HEX_HEIGHT * HEX_ROW) * j;
}

/**
 * Converts the i index of a node to the position on the map
 */
export const iToPos = (i: number): number => {
    return i * (HEX_WIDTH - HEX_LINE) * .5 + HEX_LINE * .5;
}

/**
 * Converts the j index of a node to the position on the map
 */
export const jToPos = (j: number, evenRow: boolean): number => {
    var ydiff = 0;
    if (evenRow) {
        if (isEven(j))
            ydiff = (1 - HEX_ROW) * HEX_HEIGHT;
    } else {
        if (!isEven(j))
            ydiff = (1 - HEX_ROW) * HEX_HEIGHT;
    }

    return j * (HEX_HEIGHT - 24) + ydiff;
}

/**
 * Converts physic x-coordinates to x index
 */
export const posToI = (x: number): number => {
    return Math.floor((x + HEX_WIDTH * .25 - HEX_LINE * .5) / ((HEX_WIDTH - HEX_LINE) * .5));
}

/**
 * Converts physic y-coordinates to y index
 */
export const posToJ = (y: number): number => {
    return Math.floor((y + HEX_HEIGHT * (1 - HEX_ROW) - HEX_LINE * .5) / (HEX_HEIGHT * HEX_ROW));
}

export class Point {
    public x: number = 0;
    public y: number = 0;

    public static fromIndex(i: number, j: number): Point {
        let point = new Point();
        point.x = iToPos(i);
        point.y = jToPos(j, isEven(i));
        return point;
    }

    public static fromPosition(x: number, y: number): Point {
        let point = new Point();
        point.x = posToI(x);
        point.y = posToJ(y);
        return point;
    }
}