import { Root } from "./roots";

/**
 * Handles the information on the points where you can place roots (the hexagons corners)
 */
export class Nodes {
    private nodes: Node[][];

    /**
     * Width and height provided referes to the amount of hexagons
     */
    constructor(width: number, height: number) {
        this.nodes = new Array<Array<Node>>(width * 2)
        for (let i = 0; i < width * 2; i++) {
            this.nodes[i] = new Array<Node>(height * 2);
            for (let j = 0; j < height * 2; j++) {
                this.nodes[i][j] = new Node();
            }
        }
    }

    /**
     * Checks it there is a root that ends in the node
     */
    public haveNodeRootIn(i: number, j: number): boolean {
        return this.nodes[i][j] !== undefined
            && this.nodes[i][j].in !== null;
    }

    public haveStone(i: number, j: number): boolean {
        return this.nodes[i][j].stone;
    }

    public addStone(i: number, j: number): void {
        this.nodes[i][j].stone = true;
    }

    public addRoot(root: Root): void {
        let node = this.nodes[root.iTo][root.jTo];

        if (root.iFrom >= 0 && root.jFrom >= 0)
            this.nodes[root.iFrom][root.jFrom].out.push(root);
        if (root.iTo >= 0 && root.jTo >= 0) {
            node.in = root;
        }
    }
}

export class Node {
    public in: Root;
    public out: Root[];

    public stone: boolean = false;

    constructor() {
        this.in = null;
        this.out = new Array<Root>();
    }
}

