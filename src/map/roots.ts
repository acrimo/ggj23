/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameScene } from "../scenes/game-scene"

export class Roots {
    private roots: Root[];
    constructor(scene: GameScene) {
        this.roots = new Array<Root>();
    }

    public addRoot(iStart: number, jStart: number, iEnd: number, jEnd: number): Root {
        let newRoot: Root = { iFrom: iStart, jFrom: jStart, iTo: iEnd, jTo: jEnd }
        this.roots.push(newRoot);

        return newRoot;
    }
}

export class Root {
    public iFrom: number;
    public jFrom: number;
    public iTo: number;
    public jTo: number;
}