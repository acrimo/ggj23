/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { Scene } from "phaser";
import {
    HEX_COLUMNS_COUNT,
    HEX_HEIGHT,
    HEX_LINE,
    HEX_ROW,
    HEX_ROWS_COUNT,
    HEX_WIDTH,
    SCENE_FADE_DURATION
} from "../config/const";

export class CameraController {
    private camera: Phaser.Cameras.Scene2D.Camera;

    constructor(camera: Phaser.Cameras.Scene2D.Camera) {
        this.camera = camera;

        // initial position for camera
        this.camera.scrollX = (HEX_COLUMNS_COUNT * .5 + .5) * (HEX_WIDTH - HEX_LINE)
            - this.camera.width * .5;
        this.camera.scrollY = -150;

        // play fade in
        this.camera.fadeIn(SCENE_FADE_DURATION);
    }

    public move(x: number, y: number): void {
        this.camera.scrollX -= x;
        this.camera.scrollY -= y;

        // boundaries
        if (this.camera.scrollY < -150) this.camera.scrollY = -150;
        else if (this.camera.scrollY > HEX_HEIGHT * HEX_ROW * (HEX_ROWS_COUNT - 1) + HEX_HEIGHT - this.camera.height)
            this.camera.scrollY = HEX_HEIGHT * HEX_ROW * (HEX_ROWS_COUNT - 1) + HEX_HEIGHT - this.camera.height;

        if (this.camera.scrollX < -(HEX_WIDTH - HEX_LINE) * .5) this.camera.scrollX = -(HEX_WIDTH - HEX_LINE) * .5;
        else if (this.camera.scrollX > (HEX_WIDTH - HEX_LINE) * HEX_COLUMNS_COUNT + HEX_LINE - this.camera.width + (HEX_WIDTH - HEX_LINE) * .5) {
            this.camera.scrollX = (HEX_WIDTH - HEX_LINE) * HEX_COLUMNS_COUNT + HEX_LINE - this.camera.width + (HEX_WIDTH - HEX_LINE) * .5;
        }
    }
}