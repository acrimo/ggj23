import { GameScene } from "../scenes/game-scene";
import { isEven } from "../utils/misc";
import { Root, Roots } from "../map/roots";
import { Nodes } from "../map/nodes";
import { Hexagons } from "../map/hexagons";
import { MapGraphics } from "../graphics/map-graphics";
import { iToPos, jToPos, xPosHexa, yPosHexa } from "../map/coordinates";

import {
    HEX_WIDTH,
    HEX_LINE,
    HEX_ROW,
    HEX_HEIGHT,
    HEX_COLUMNS_COUNT,
    HEX_ROWS_COUNT
} from "../config/const";

export class MapController {
    private scene: GameScene;

    private nodes: Nodes;
    private hexagons: Hexagons;

    public roots: Roots;

    constructor(scene: GameScene) {
        this.scene = scene;

        this.hexagons = new Hexagons(HEX_COLUMNS_COUNT, HEX_ROWS_COUNT);
        this.nodes = new Nodes(HEX_COLUMNS_COUNT, HEX_ROWS_COUNT);

        this.roots = new Roots(scene);
    }

    /**
     * Pseudo procedural generation for the map based on distance 
     * to the starting point to calculate prob to place water/nutrients
     * and obstacles
     */
    private generateRandomMap(graphics: MapGraphics): void {
        const SQR_DIS_ROCKS = 1 / (30 * 30 + 60 * 60);
        const SQR_DIS_WATER = 1 / (15 * 15 + 30 * 30);

        let previousLine = new Array<boolean>(30);
        let currentLine = new Array<boolean>(30);
        for (let j = 0; j < 30; j++) {
            previousLine[j] = false;
            currentLine[j] = false;
        }

        // pseudo procedural generation for the map
        for (let j = 1; j < 29; j++) {
            for (let i = 1; i < 29; i++) {
                if (j % 2 == 0) { // same and plus 1
                    if (!(previousLine[i] || previousLine[i + 1] || currentLine[i - 1])) {
                        var roll = Math.random();
                        if (roll < 0.2) {
                            let image = graphics.addWater(xPosHexa(i, j), yPosHexa(j));
                            this.hexagons.addWater(i, j, image);

                            currentLine[i] = true;
                        }
                        else if (roll < 0.4) {
                            let image = graphics.addNutrient(xPosHexa(i, j), yPosHexa(j));
                            this.hexagons.addNutrient(i, j, image);

                            currentLine[i] = true;
                        } else {
                            currentLine[i] = false;
                        }
                    } else {
                        currentLine[i] = false;
                    }
                } else {
                    if (!(currentLine[i] || currentLine[i - 1] || previousLine[i - 1])) {
                        var roll = Math.random();
                        if (roll < 0.2) {
                            let image = graphics.addWater(xPosHexa(i, j), yPosHexa(j));
                            this.hexagons.addWater(i, j, image);

                            previousLine[i] = true;
                        }
                        else if (roll < 0.4) {
                            let image = graphics.addNutrient(xPosHexa(i, j), yPosHexa(j));
                            this.hexagons.addNutrient(i, j, image);

                            previousLine[i] = true;
                        } else {
                            previousLine[i] = false;
                        }
                    } else {
                        previousLine[i] = false;
                    }
                }
            }
        }

        // let count = 0;
        for (let i = 0; i < 60; i++) {
            for (let j = 0; j < 60; j++) {
                if (!(j < 2 && i > 27 && i < 33)) {
                    var prob = .1 + ((i - 30) * (i - 30) + j * j) * SQR_DIS_ROCKS * .7;

                    if (Math.random() < prob) {
                        let image = graphics.addStone(iToPos(i), jToPos(j, isEven(i)));
                        this.nodes.addStone(i, j);
                    }
                }
            }
        }
    }

    /**
     * Start the map data and creates the graphics for all rendeareable items
     */
    public start(data: any, graphics: MapGraphics) {
        if (data == null) {
            this.generateRandomMap(graphics);
        } else {
            // load map on data
            // this.gameData.waters.forEach(e => {
            //     this.waters.add(e.hexa_x, e.hexa_y);
            // });
            // this.gameData.nutrients.forEach(e => {
            //     this.nutrients.add(e.hexa_x, e.hexa_y);
            // });

            // this.gameData.waters.forEach(e => {
            //     let collectable = new Water(e.hexa_x, e.hexa_y);
            //     this.waters.add(collectable);
            //     this.grid.setCollectable(collectable.getAffectedNodes());
            // });
            // this.gameData.nutrients.forEach(e => {
            //     let collectable = new Nutrient(e.hexa_x, e.hexa_y);
            //     this.nutrients.add(collectable);
            //     this.grid.setCollectable(collectable.getAffectedNodes());
            // });
        }

        // inital root
        let startPoint = 31;
        this.addRoot(startPoint, -1, startPoint, 0);
        graphics.addRoot(
            iToPos(startPoint), jToPos(-1, isEven(startPoint)),
            iToPos(startPoint), jToPos(0, isEven(startPoint))
        );
    }

    /**
     * Checks if node (x0, y0) has a connection to node (x1, y1)
     */
    public isConnected(i0: number, j0: number, i1: number, j1: number): boolean {
        let dx = i1 - i0; let dy = j1 - j0;

        if (Math.abs(dx) > 1 || Math.abs(dy) > 1 || Math.abs(dx) + Math.abs(dy) > 1) return false; //distance check
        // if (this.haveStone(x1, y1)) return false; // has stone
        if (dy == 0) return true; // at same row
        if (dx != 0) return false; // if different row, only up-down checking

        if (isEven(j0)) { // even row
            if (isEven(i0) && dy > 0) // even columns, moving down
                return true;
            if (!isEven(i0) && dy < 0) // odd columns, moving up
                return true;
        } else { // odd row
            if (isEven(i0) && dy < 0) // even columns, moving up
                return true;
            if (!isEven(i0) && dy > 0) // odd columns, moving down
                return true;
        }

        return false;
    }

    /**
     * Checks if the node in the i,j position have a root ending that can start another
     * root segment. Refer to game rules for more accurate info about it
     */
    public canStartRoot(i: number, j: number): boolean {
        return this.nodes.haveNodeRootIn(i, j);
    }

    public addRoot(i0: number, j0: number, i1: number, j1: number): boolean {
        let root = this.roots.addRoot(i0, j0, i1, j1);
        this.nodes.addRoot(root);

        // check if consume around
        this.consume(i1, j1);

        return true;
    }

    private consume(i: number, j: number): void {
        // common first
        let i0 = Math.floor(i * .5);

        let j2 = j; let i2 = i0;
        if (isEven(i)) {
            i2 -= 1;
            if (!isEven(j)) j2 -= 1;
        }
        else {
            i2 += 1;
            if (isEven(j)) j2 -= 1;
        }

        // if (!isEven(j) && !isEven(i)) { // x odd, y odd
        //     i3 = floor + 1;
        // }
        // if (isEven(i) && !isEven(j)) { // x even, y odd
        //     i3 = floor - 1;
        //     j3 = j3 - 1;
        // }
        // if (!isEven(i) && isEven(j)) { // x odd, y even
        //     i3 = floor + 1;

        //     j3 = j3 - 1;
        // }
        // if (isEven(i) && isEven(j)) { // x even, y even
        //     i3 = floor - 1;
        // }

        [
            this.hexagons.consume(i0, j),
            this.hexagons.consume(i0, j - 1),
            this.hexagons.consume(i2, j2)
        ]
            .forEach((result: number) => {
                if (result == 1) this.scene.consumeWater();
                else if (result == 2) this.scene.consumeNutrient();
            });
    }

    public haveStone(i: number, j: number): boolean {
        return this.nodes.haveStone(i, j);
    }

    public haveNodeRootIn(i: number, j: number): boolean {
        return this.nodes.haveNodeRootIn(i, j);
    }

    // /**** DEBUG ****/
    // private drawGrid(): void {
    //     var debugGraphics = this.add.graphics();
    //     debugGraphics.lineStyle(1, 0xff0000);

    //     for (let i = 0; i < 10; i++) {
    //         let y = (HEX_HEIGHT * HEX_ROW) * i //step
    //             - HEX_HEIGHT * (1 - HEX_ROW) //row correction
    //             + HEX_LINE * .5; //line correction
    //         debugGraphics.lineBetween(0, y, 960, y);
    //         let x = ((HEX_WIDTH - HEX_LINE) * .5) * i //step
    //             - HEX_WIDTH * .25 //row correction
    //             + HEX_LINE * .5; //line conecction
    //         debugGraphics.lineBetween(x, 0, x, 540);
    //     }
    // }
}