/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { Scene } from "phaser";
import { MUSIC_FADE_DURATION } from "../config/const"

/**
 * Handles background music for the entire game
 */
export class MusicController {
    private currentMusic: Phaser.Sound.BaseSound;

    private tracks: Phaser.Sound.BaseSound[];
    private upgrade: Phaser.Sound.BaseSound;

    // private track: Phaser.Sound.BaseSound;

    private index: number = -1;

    constructor(scene: Scene) {
        this.tracks = new Array<Phaser.Sound.BaseSound>(3);

        for (let i = 0; i < 3; i++)
            this.tracks[i] = scene.sound.add("track-" + i, { loop: true });

        this.upgrade = scene.sound.add("track-upgrade", { loop: true });

        this.currentMusic = null;
    }

    /**
     * Creates a one time use tween to raise the volume
     * from 0 to 1 over the duration of MUSIC_FADE
     */
    private fadeIn(scene: Scene) {
        scene.tweens.add({
            targets: this.currentMusic,
            duration: MUSIC_FADE_DURATION,
            volume: 1
        });

        this.currentMusic.play({ volume: 0 });
    }

    /**
     * Creates a one time use tween to raise the volume
     * from 1 to 0 over the duration of MUSIC_FADE
     */
    public fadeOut(scene: Scene): void {
        if (this.currentMusic == null) return;

        scene.tweens.add({
            targets: this.currentMusic,
            duration: MUSIC_FADE_DURATION,
            volume: 0,
            onComplete: () => {
                this.currentMusic.stop();
            }
        });
    }

    /**
     * Selects one of the 2 available tracks (index 1 is for main menu) 
     * and plays it with a fade in
     */
    public playRandom(scene) {
        this.index = Math.random() > 0.5 ? 0 : 2; // 1 is for menu

        this.currentMusic = this.tracks[this.index];
        this.fadeIn(scene);
    }

    /**
     * Plays second track with a fade in
     */
    public startMenu(scene: Phaser.Scene): void {
        this.currentMusic = this.tracks[1];
        this.fadeIn(scene);
    }
}