/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { GameScene } from "../scenes/game-scene";
import { UpgradeScene } from "../scenes/upgrades-scene";

import { INITIAL_MOVES } from "../config/const";

export enum LevelState {
    LOADING = 0
}

export class LevelController {
    private state: LevelState;

    private game: GameScene;
    private upgrade: UpgradeScene;

    private movementCount: number;
    private waterCount: number;
    private nutrientCount: number;

    constructor(game: GameScene, upgrade: UpgradeScene) {
        this.game = game;
        this.upgrade = upgrade;
    }

    public start(data: any): void {
        this.game.start(data, this);
        this.upgrade.start(data);

        this.waterCount = 0;
        this.nutrientCount = 0;

        this.movementCount = INITIAL_MOVES;

        this.state = LevelState.LOADING;
    }

    public getWaterCount(): number { return this.waterCount; }

    public addWater(): number { return ++this.waterCount; }

    public getNutrient(): number { return this.nutrientCount; }

    public addNutrient(): number { return ++this.nutrientCount; }

    public useMove(): number { return --this.movementCount; }
}
