/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { Scene } from "phaser";
import { GameScene } from "../scenes/game-scene";
import { UpgradeScene } from "../scenes/upgrades-scene";
import { MusicController } from "./music"
import { LevelController } from "./level";

/**
 * 
 */
export class GameController {
    //#region singleton

    private static instance: GameController;

    public static getInstance(): GameController {
        if (!GameController.instance) {
            GameController.instance = new GameController();
        }

        return GameController.instance;
    }

    private constructor() { }

    //#endregion

    private levelData: any = null;

    private currentScene: Scene;
    private level: LevelController;

    private music: MusicController;

    public getCurrentScene(): Scene { return this.currentScene; }
    private setCurrentScene(scene: Scene) { this.currentScene = scene; }

    public setLevelData(levelData: any): void {
        // loadJSON('./assets/config/level-0.json', (response) => {
        //     this.gameData = JSON.parse(response);
        //     // initial state // after json being loaded
        //     this.start();
        // });

        this.levelData = levelData;
    }

    public onMenuStart(scene: Scene): void {
        this.setCurrentScene(scene);

        // creates a music instance if not created already.
        // menu will always be started as the first scene with music
        if (!this.music) { this.music = new MusicController(scene); }

        this.music.startMenu(scene);
    }

    public onMenuClose(scene: Scene): void {
        this.music.fadeOut(scene);
    }

    public onGameSceneCreate(gameScene: GameScene): void {
        this.currentScene = gameScene;

        // creates both the scenes for level and then level manager
        gameScene.scene.launch("UpgradeScene"); // start a scene and renders on top of this one
        this.level = new LevelController(gameScene, gameScene.scene.get("UpgradeScene") as UpgradeScene);

        this.music.playRandom(gameScene);

        // setup and starts up the game
        this.level.start(this.levelData);
    }
}