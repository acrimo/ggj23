/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

// export class TextImageButton extends Phaser.GameObjects.Image {

//     constructor(scene: Phaser.Scene, x: number, y: number,
//         idle: string, hover: string, callback: () => void) {
//         super(scene, x, y, idle);

//         this.setInteractive();
//         this.on('pointerover', () => this.setTexture(hover));
//         this.on('pointerout', () => this.setTexture(idle));
//         this.on('pointerdown', () => callback());
//         // this.on('pointerup', () => this.startCallback());

//         scene.add.existing(this);
//     }
// }

// export class TextButton extends Phaser.GameObjects.BitmapText {
//     private initialX: number;
//     private initialY: number;

//     callback: () => void;

//     constructor(scene: Phaser.Scene, x: number, y: number,
//         font: string, size: number, width: number, height: number,
//         callback: () => void) {
//         super(scene, x, y, font, '', size);

//         this.on('pointerover', this.onPointerOver);
//         this.on('pointerout', this.onPointerOut);
//         this.on('pointerdown', this.onPointerDown);
//         this.on('pointerup', this.onPointerUp);

//         this.initialX = x;
//         this.initialY = y;
//         this.callback = callback;

//         this.setInteractive(new Phaser.Geom.Rectangle(0, 0,
//             width, height), Phaser.Geom.Rectangle.Contains);

//         scene.add.existing(this);
//     }

//     private onPointerOver(): void {
//         // this.x -= this.width * 0.05;
//         this.y = this.initialY - this.height * 0.05;
//         this.setScale(1.1, 1.1);
//     }

//     private onPointerOut(): void {
//         if (this.input.enabled) {
//             this.setScale(1, 1);
//             this.y = this.initialY;
//         }
//     }

//     private onPointerDown(): void {
//         this.setScale(0.95, 0.95);
//         this.y = this.initialY + this.height * 0.025;
//     }

//     private onPointerUp() {
//         this.setScale(1.1, 1.1);
//         this.y = this.initialY - this.height * 0.05;

//         this.callback();
//     }

//     /**
//      * Sets a new string for the button,
//      * activate it and recalculate the
//      * shape of interaction.
//      * @param content String to be setted
//      */
//     public setTextAndShape(content: string): TextButton {
//         this.text = content;
//         this.visible = true;

//         this.setInteractive();
//         return this;
//     }

//     /**
//      * Reset the button to initial state:
//      * - Interactive: enable
//      * - Size: original
//      */
//     public reset(): void {
//         this.setInteractive();
//         this.setScale(1, 1);
//         this.y = this.initialY;
//     }
// }

// export class ImageButton extends Phaser.GameObjects.Image {
//     callback: () => void;

//     private idle: string;
//     private hover: string;
//     private hold: string;

//     constructor(scene: Phaser.Scene, x: number, y: number,
//         idle: string, hover: string, hold: string,
//         callback: () => void) {
//         super(scene, x, y, idle);

//         this.on('pointerover', this.onPointerOver);
//         this.on('pointerout', this.onPointerOut);
//         this.on('pointerdown', this.onPointerDown);
//         this.on('pointerup', this.onPointerUp);

//         this.idle = idle;
//         this.hover = hover;
//         this.hold = hold;

//         this.setInteractive();
//         this.callback = callback;

//         scene.add.existing(this);
//     }

//     private onPointerOver(): void {
//         if (this.hover)
//             this.setTexture(this.hover);
//         else
//             this.setScale(1.1, 1.1);
//     }

//     private onPointerOut(): void {

//         if (this.input.enabled) {
//             this.setTexture(this.idle);
//             this.setScale(1, 1);
//         }
//     }

//     private onPointerDown(): void {
//         if (this.hold)
//             this.setTexture(this.hold)
//         else
//             this.setScale(0.95, 0.95);
//     }

//     private onPointerUp() {
//         if (this.hover)
//             this.setTexture(this.idle);
//         else {
//             this.setTexture(this.idle);
//             this.setScale(1.1, 1.1);
//         }

//         this.callback();
//     }
// }

export abstract class Button {
    protected callback: () => void;

    constructor(scene: Phaser.Scene, callback: () => void) {
        this.callback = callback;
    }

    protected setCallbacks(object: Phaser.GameObjects.GameObject): void {
        object.on('pointerover', this.onPointerOver, this);
        object.on('pointerout', this.onPointerOut, this);
        object.on('pointerdown', this.onPointerDown, this);
        object.on('pointerup', this.onPointerUp, this);

        object.setInteractive();
    }

    protected abstract onPointerOver(): void;
    protected abstract onPointerOut(): void;
    protected abstract onPointerDown(): void;
    protected onPointerUp(): void { this.callback(); }
}

export class ImageButton extends Button {
    protected background: Phaser.GameObjects.Image;

    private scaleUp: number;
    private scaleDown: number;

    constructor(scene: Phaser.Scene, callback: () => void,
        texture: string, x: number, y: number,
        scaleUp: number = 1.1, scaleDown: number = 0.95) {

        super(scene, callback);

        this.background = scene.add.image(x, y, texture);

        this.setCallbacks(this.background);

        this.scaleDown = scaleDown; this.scaleUp = scaleUp;
    }

    protected onPointerOver(): void {
        this.background.setScale(this.scaleUp);
    }

    protected onPointerOut(): void {
        this.background.setScale(1, 1);
    }

    protected onPointerDown(): void {
        this.background.setScale(this.scaleDown);
    }

    protected onPointerUp() {
        this.background.setScale(this.scaleUp);
        super.onPointerUp();
    }
}

export class TextButton extends Button {
    private text: Phaser.GameObjects.BitmapText;

    private normalTint: number;
    private hoverTint: number;
    private clickTint: number;

    constructor(scene: Phaser.Scene, callback: () => void,
        content: string, x: number, y: number,
        normalTint: number = 0xffffff,
        hoverTint: number = 0xffffff,
        clickTint: number = 0xffffff,
        scaleUp: number = 1.1, scaleDown: number = 0.95) {

        super(scene, callback);

        this.normalTint = normalTint;
        this.hoverTint = hoverTint;
        this.clickTint = clickTint;

        this.text = scene.add.bitmapText(x, y, "orbitron", content, 25);
        this.text.setOrigin(0.5);
        this.setCallbacks(this.text);
    }

    protected onPointerOver(): void {
        this.text.setScale(1.1);
        this.text.setTint(this.hoverTint);
    }

    protected onPointerOut(): void {
        this.text.setScale(1);
        this.text.setTint(this.normalTint);
    }

    protected onPointerDown(): void {
        this.text.setScale(0.95);
        this.text.setTint(this.clickTint);
    }

    protected onPointerUp() {
        this.text.setScale(1.1);
        this.text.setTint(this.hoverTint);
        super.onPointerUp();
    }
}