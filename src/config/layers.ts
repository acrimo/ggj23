
export const BACKGROUND_LAYER: number = 0;
export const TREE_LAYER: number = 10;

export const HEXAGONS_LAYER: number = 100;
export const ROCKS_LAYER: number = 110;

export const COLLECTABLE_LAYER: number = 200;

export const STONE_LAYER: number = 300;
export const ROOT_LAYER: number = 350;

export const MARKER_LAYER: number = 400;

export const UI_LAYER: number = 1000;