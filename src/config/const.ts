export const MUSIC_FADE_DURATION = 400;
export const SCENE_FADE_DURATION = 500;

// sizes of the hexagon, swaping this values will automatically 
// change the way they are printed and the way the colision are calculated
export const HEX_WIDTH = 80;
export const HEX_HEIGHT = 90;
export const HEX_LINE = 2;
export const HEX_ROW = .7375;

export const HEX_COLUMNS_COUNT = 30;
export const HEX_ROWS_COUNT = 30;

export const SQR_TRESHOLD_DRAG = 20 * 20;

export const INITIAL_MOVES = 10;