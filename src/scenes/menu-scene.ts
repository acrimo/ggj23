import { SCENE_FADE_DURATION } from "../config/const";
import { GameController } from "../controllers/game"
import { ImageButton } from "../utils/button"

export class MenuScene extends Phaser.Scene {
    constructor() {
        super({
            key: "MenuScene"
        });
    }

    create(): void {
        this.cameras.main.backgroundColor = new Phaser.Display.Color(157, 127, 85);

        new ImageButton(this,
            () => { this.startCallback(); },
            "play-button",
            this.cameras.main.width * 0.5,
            this.cameras.main.height * 0.5
        );

        this.playFadeEnterAnimation();

        GameController.getInstance().onMenuStart(this);
    }

    private playFadeEnterAnimation(): void {
        this.cameras.main.fadeIn(SCENE_FADE_DURATION);
    }

    private startCallback(): void {
        GameController.getInstance().onMenuClose(this);
        this.cameras.main.fadeOut(SCENE_FADE_DURATION);

        this.time.delayedCall(SCENE_FADE_DURATION, () => {
            this.scene.start("GameScene");
        });
    }
}