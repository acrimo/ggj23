/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { MenuScene } from './menu-scene';

export class LoadScene extends Phaser.Scene {
    private progressBar: Phaser.GameObjects.Image;
    // private platformData = null;

    constructor() {
        super({
            key: "LoadScene"
        });
    }

    /**
     * Only load the assets to make the load screen.
     */
    private preload(): void {
        // setup prefix for loading assets
        this.load.setBaseURL('./assets/');

        // load assets
        // this.load.image('tile-background', 'sprites/background.png');

        // progress bar
        this.add.graphics()
            .fillStyle(0x5dae47, 1)
            .fillRect(0, 0, this.cameras.main.width * .5, 20)
            .generateTexture('loading-bar', this.cameras.main.width * .5, 20)
            .clear()
            .fillStyle(0xfff6d3, 1)
            .fillRect(0, 0, 1, 16)
            .generateTexture('progress-bar', 1, 16)
            .destroy();
    }

    private create(): void {
        // set the background and create loading bar
        this.cameras.main.setBackgroundColor(0x98d687);
        // this.add.tileSprite(0, 0, 540, 960, 'tile-background').setOrigin(0, 0);

        // loading bar
        this.add.image(this.cameras.main.width * .25,
            this.cameras.main.height * .5,
            'loading-bar').setOrigin(0, .5);
        this.progressBar = this.add.image(this.cameras.main.width * .25 + 2,
            this.cameras.main.height * .5,
            'progress-bar').setOrigin(0, .5);

        // pass value to change the loading bar fill
        this.load.on("progress", (value) =>
            this.progressBar.setScale(this.cameras.main.width * .5 * value - 4, 0)
        );

        this.load.on("complete", () => {
            // after loading all assets create the animations here
            // so its only happen once
            // this.anims.create({
            //     key: 'win',
            //     frames: this.anims.generateFrameNumbers('blue-anim', {
            //         start: 0, end: 75
            //         //frames: [26, 27, 28, 18, 17, 16]
            //     }),
            //     yoyo: true,
            //     repeat: -1,
            //     frameRate: 30
            // });

            // fade screen
            this.cameras.main.fadeOut(500);

            // after fade animation start menu scene
            this.time.delayedCall(500, this.scene.start,
                ["MenuScene"], this.scene);
        }, this);

        // list of assets to be loaded
        this.load.image('play-button', 'sprites/play-button.png');

        // // generic
        // this.load.spritesheet('blue-anim', 'sprites/blue-anim.png', {
        //     frameWidth: 256, frameHeight: 256
        // });

        this.load.audio('track-0', 'audio/track-0.mp3');
        this.load.audio('track-1', 'audio/track-1.mp3');
        this.load.audio('track-2', 'audio/track-2.mp3');
        this.load.audio('track-upgrade', 'audio/night.mp3');


        // this.load.atlas('spritesheet', 'sprites/spritesheet.png', 'sprites/spritesheet.json');

        // pancho's assets
        this.load.image("hexagon", "sprites/hexagon.png");
        this.load.image("marker", "sprites/marker.png");

        this.load.image("tree", "sprites/tree-background.png")

        this.load.image("ui-container-big", "sprites/ui-container-big.png");
        this.load.image("ui-container-small", "sprites/ui-container-small.png");

        this.load.image("ui-moves", "sprites/ui-moves.png");
        this.load.image("ui-nutrient", "sprites/ui-nutrient.png");
        this.load.image("ui-water", "sprites/ui-water.png");

        this.load.image("rock", "sprites/rock.png");
        this.load.image("skeleton", "sprites/skeleton.png");

        this.add.graphics()
            .fillStyle(0xfbfbfb, 1)
            .fillRect(0, 0, 177, 25)
            .generateTexture('ui-water-background', 177, 25)
            .clear()
            .fillStyle(0xB6F1F5, 1)
            .fillRect(0, 0, 1, 25)
            .generateTexture('ui-water-fill', 1, 25)
            .fillStyle(0xFF0000, 1)
            .fillRect(0, 0, 1, 25)
            .generateTexture('ui-water-dead', 1, 25)
            .destroy();

        this.load.bitmapFont("font", "fonts/font.png", "fonts/font.xml");

        this.load.image("dirt", "sprites/dirt-background.png");

        this.load.image("nutrient", "sprites/nutrient.png");
        this.load.image("water", "sprites/water.png");

        this.load.image("tree-back-0", "sprites/background/ground.png");
        this.load.image("tree-back-1", "sprites/background/sky.png");
        this.load.image("tree-back-2", "sprites/background/treesBack.png");
        this.load.image("tree-back-3", "sprites/background/treesFront.png");
        this.load.image("tree-back-4", "sprites/background/bushes.png");
        this.load.image("tree-back-5", "sprites/background/leaves.png");

        this.load.image("tree-upgrade", "sprites/abilityTree/7.png");

        this.load.image("hex-tile", "sprites/hexagon-grid.png");

        this.load.image("upgrade", "sprites/upgrade-icon.png");

        this.load.image("next-day-button", "sprites/next-day-button.png");

        this.load.image("upgrade-0", "sprites/upgrade-0.png");
        this.load.image("upgrade-1", "sprites/upgrade-1.png");
        this.load.image("upgrade-2", "sprites/upgrade-2.png");
        this.load.image("upgrade-3", "sprites/upgrade-3.png");
        this.load.image("upgrade-4", "sprites/upgrade-4.png");

        this.load.image("buy-button", "sprites/buy-button.png");

        // restart load process
        this.load.start();
    }
}