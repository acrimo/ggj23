/// <reference path='../../node_modules/phaser/types/phaser.d.ts'/>

import { LevelController } from "../controllers/level";
import { GameController } from "../controllers/game";

import { GameInput } from "../input/game-input"
import { UI } from "../objects/ui"

import { CameraController } from "../controllers/camera";
import { MapController } from "../controllers/map"

import { MapGraphics } from "../graphics/map-graphics";
import { Marker } from "../graphics/marker";

import { iToPos, jToPos, posToI, posToJ, xPosHexa, yPosHexa } from "../map/coordinates";
import { isEven } from "../utils/misc";
import { Game } from "phaser";

export enum GameState {
    INIT,
    BUILD,
    BUILD_DRAG,
    PREUPGRADE,
    UPGRADE_DRAG,
    UPGRADE,
    END,
    DRAG
}

export class GameScene extends Phaser.Scene {

    private level: LevelController;

    private camera: CameraController;
    private ui: UI;

    private gameInput: GameInput;

    private map: MapController;
    private mapGraphics: MapGraphics;
    private marker: Marker;

    // state machine
    private state: GameState;

    constructor() { super({ key: "GameScene" }); }

    private create(): void {
        this.camera = new CameraController(this.cameras.main);

        this.map = new MapController(this);

        this.mapGraphics = new MapGraphics(this);
        this.marker = new Marker(this, this.mapGraphics);

        this.gameInput = new GameInput(this);

        this.ui = new UI(this);

        GameController.getInstance().onGameSceneCreate(this);
    }

    public start(data: any, level: LevelController) {
        this.level = level;

        this.map.start(data, this.mapGraphics);

        this.ui.setFilledPerc(0);
        this.ui.setNutrientCount(0);

        // initial state
        this.state = GameState.BUILD;
    }

    public update(time: number, delta: number): void {
    }

    //#region State

    public getState(): GameState {
        if (this.state == GameState.BUILD_DRAG
            || this.state == GameState.UPGRADE_DRAG) {
            return GameState.DRAG;
        }

        return this.state;
    }

    public startDrag(): void {
        this.marker.clear();

        if (this.state == GameState.BUILD)
            this.state = GameState.BUILD_DRAG;
        else if (this.state == GameState.PREUPGRADE)
            this.state = GameState.UPGRADE_DRAG
    }

    public startBuild(): void {
        this.marker.clear();

        this.state = GameState.BUILD;
    }

    private endBuild(): void {
        this.marker.clear();

        // this.ui.showUpButton();

        this.state = GameState.PREUPGRADE;
    }

    public endUpgrade(): void {
        this.state = GameState.BUILD;
    }

    //#endregion

    //#region Camera

    public moveCamera(x: number, y: number) {
        this.camera.move(x, y);
    }

    //#endregion

    //#region Marker

    public moveMarker(i: number, j: number) {
        if (!this.marker.isFirstLocked()) {
            // no point is selected so we are looking for a node that has a roots end
            if (this.map.canStartRoot(i, j))
                this.marker.move(iToPos(i), jToPos(j, isEven(i)));
            else
                this.marker.hideFirst();
        } else {
            // we already have the first node selected
            let pos = this.marker.getLockedPosition();
            if (this.map.isConnected(pos.x, pos.y, i, j)
                && !this.map.haveStone(i, j)
                && !this.map.haveNodeRootIn(i, j)) {
                this.marker.move(iToPos(i), jToPos(j, isEven(i)));
            } else {
                this.marker.hideFirst();
            }
        }
    }

    public placeMarker(): void {
        if (this.marker.rootReady()) {
            // if we have 2 nodes marked try to create a root between them
            this.addRoot();
        }
        else if (this.marker.isPointerOnFirst()) {
            // if pointer is on top of a viable first node highlights it
            this.marker.select();
        } else {
            this.marker.clear();
        }
    }

    //#endregion

    //#region Map callbacks

    private addRoot(): void {
        // check if movements available
        // ...

        let first = this.marker.getLockedPosition();
        let second = this.marker.getSecondPosition();

        if (this.map.addRoot(first.x, first.y, second.x, second.y)) {
            this.mapGraphics.addRoot(
                iToPos(first.x), jToPos(first.y, isEven(first.x)),
                iToPos(second.x), jToPos(second.y, isEven(second.x)),
            );

            let remainingMoves = this.level.useMove();
            this.ui.setMoveCount(remainingMoves);

            if (remainingMoves == 0) {
                this.endBuild();
            } else {

                this.marker.clear();
                // mark the end of the root as the start for the next one
                this.marker.select();
            }
        }

        // if (this.level.movementCount == 0) {
        //     setTimeout(() => {
        //         // this.tree.goUp();
        //         this.state = GameState.UPGRADE;

        //         // this.input.pointerClear();
        //     }, 500);
        // }
    }

    public consumeWater(): void {
        let water = this.level.addWater();

        this.ui.setFilledPerc(water);
    }

    public consumeNutrient(): void {
        let nutrient = this.level.addNutrient();

        this.ui.setNutrientCount(nutrient);
    }

    //#endregion
}